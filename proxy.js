var superagent = require('superagent');
var exec = require('child_process').exec;
var spawn = require('child_process').spawn;
var Sugar = require('sugar');
Sugar.extend();
var RequestPool = require('../lib/request-pool');
var md5File = require('md5-file');
var fs = require('fs-extra');
//var Meo = require('meo-u/meo');
var _ = require('lodash');
var ps = require('ps-node');
var path = require('path');

process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;

var serverName = process.argv[2] ? process.argv[2].toUpperCase() : undefined;
if (!serverName) throw 'need to provide server via arguments!';
var config = fs.readJSONSync('src/config.json');
if (!config[serverName]) throw 'server is not the config list!';

var serverConfig = config[serverName];

//Pusher = new Meo.Pushover({
//    token: 'aHsrCiy75vf9CJkK19mSEzvivyRX5f',
//    keyGroup: 'gaHyJYXuaJPQ22dPb94EXs8EpgVvmz'
//});

var MODIFIED_LEAGUECLIENT_MD5 = 'f9e2661537ad24960e8f432ff21e5752';
// var LEAGUECLIENT_DIR = serverName === 'VN' ? 'C:\\Program Files (x86)\\LeagueClient\\' : 'Z:\\Program Files (x86)\\LeagueClient7.24\\';
if (serverName === 'VN') {
    //var LEAGUECLIENT_DIR = 'C:\\Program Files (x86)\\LeagueClient8.2\\';
    var LEAGUECLIENT_DIR = 'C:\\Program Files (x86)\\LeagueClient\\';
} else {
    // else if (serverName === 'HK' || serverName === "TW"){
    //     var LEAGUECLIENT_DIR = 'Z:\\Program Files (x86)\\LeagueClientHK\\';
    // }
    var LEAGUECLIENT_DIR = 'Z:\\Program Files (x86)\\LeagueClient\\';

    //    var LEAGUECLIENT_DIR = 'Z:\\Program Files (x86)\\LeagueClient7.24\\';
}
// var LEAGUECLIENT_DIR = 'F:\\LeagueClient\\';
var LEAGUECLIENT_DIR = 'C:\\Program Files (x86)\\LeagueClient8.2\\';

// var a = 'C:\\Program Files (x86)\\LeagueClient\\';
console.log(serverName, LEAGUECLIENT_DIR);

var requestPool = new RequestPool({
    maxRequestPerSec: 3
});

var http = require('http');
var httpProxy = require('http-proxy');

var pass = 'Abc123456';
// var pass = '123QWEasd';
var SB_PREFIX = 'sb';
var loopIsRunning = false;

// var users = [
//     'shakeekahs',
//     //'mouthhtuom',
//     'liraxlirax',
//     'shadeedahs',
//     'underrednu',
//     'shimppmihs',
//     'cleannaelc',
//     'foundfound',
//     //'hyunaanuyh',
//     'poppyyppop',

//     'zhikklequi',
//     'brucebazer',
//    // 'myzqueobou',
//     'cabilianna',
//     'shonjongki',
//    // 'macadamiad',
//     'gizzlewale',
//     'mizzacible',
//     'lisserenis',
//     'marvelousy',
// ];

// var ip = require('ip');

// var hostNumber = ip
//     .address()
//     .split('.')
//     .last();
// var hostNumber = 1;
// var users = fs
//     .readFileSync('./users/' + hostNumber + '.txt')
//     .toString()
//     .split(/\r?\n/);
var users = serverConfig.users;
console.log(users);
users = users.map(function(user, index) {
    return {
        name: user,
        index: index,
        status: false,
        ready: false
    };
});

function createDefaultSummoner(user) {
    superagent
        .post(
            'https://riot:530d6ba363b5a23e@localhost:' +
                (serverConfig.internalPort + user.index) +
                '/lol-summoner/v1/summoners'
        )
        .send({ name: 'lmssalpha' + user.name.first(3) + user.index })
        .end(function(err, res) {
            //console.log('err', res.body.message);
            // console.log(res);
            console.log( 'lmssalpha' + user.name.first(3) + user.index);
        });
}

function setUserFail(user) {
    user.status = false;
    user.ready = false;
}
function setUserConnect(user) {
    user.status = true;
    user.ready = false;
    setTimeout(function() {
        user.ready = true;
        createDefaultSummoner(user);
    }, 60 * 1000);
}

requestPool.add(users);

var proxy = httpProxy.createProxyServer({
    auth: 'riot:530d6ba363b5a23e',
    // https: true,
    // rejectUnauthorized: false,
    secure: false
});

var countrequestforminute = 0;

var server = http.createServer(function(req, res) {
    // console.log(req.url);
    // if (req.url.includes('summoner-top-champs')) {
    //        res.writeHead(500, {
    //            'Content-Type': 'text/plain'
    //        });
    //        return res.end('Something went wrong. summoner-top-champs now allowed.');
    // }

    var nextUser = requestPool.getNextAndCountRequest(function(user) {
        return user.ready;
    });

    countrequestforminute++;
    if (!nextUser) {
        res.writeHead(500, {
            'Content-Type': 'text/plain'
        });
        return res.end('Something went wrong. And we are reporting a custom error message.');
    }

    var host = 'https://127.0.0.1:' + (serverConfig.internalPort + nextUser.index);
    req.headers.index = nextUser.index;
    proxy.web(req, res, {
        target: host,
        secure: false
    });
});

proxy.on('proxyRes', function(proxyRes, req, res) {
    proxyRes.on('data', function(dataBuffer) {
        var data = dataBuffer.toString();

        // try {
        //     var data = dataBuffer.toString();
        // } catch(e) {
        //     console.log(e);
        //     return users[req.headers.index].status = false;
        //     // return proxyRes.end('error message proxyRes');
        // }

        // if (data.includes('errorCode')) {
        // console.log(data)
        // if (data.includes('errorCode')) console.log(data);

        if (data.includes('invoke lcds: 10')) {
            setUserFail(users[req.headers.index]);
            //Pusher.send(serverName + ' API server proxyRes', 'invoke lcds: 10', true);
            return console.log(data, users[req.headers.index]);
            // proxyRes.end('error message proxyRes');
        }
        if (data.includes('invoke lcds: 13')) {
            setUserFail(users[req.headers.index]);
            //Pusher.send(serverName + ' API server proxyRes', 'invoke lcds: 13', true);
            return console.log(data, users[req.headers.index]);
            // proxyRes.end('error message proxyRes');
        }
        if (data.includes('invoke lcds: 21')) {
            setUserFail(users[req.headers.index]);
            //Pusher.send(serverName + ' API server proxyRes', 'invoke lcds: 21', true);
            return console.log(data, users[req.headers.index]);
            // proxyRes.end('error message proxyRes');
        }

        if (data.includes('No login session exists')) {
            setUserFail(users[req.headers.index]);
            //Pusher.send(serverName + ' API server proxyRes', 'No login session exists', true);
            return console.log(data, users[req.headers.index]);

            // proxyRes.end('error message proxyRes');
        }

        // if (data.includes("Failed to get top champions for summoner")) {
        // 	setUserFail( users[req.headers.index])
        //     // proxyRes.end('error message proxyRes');
        // }
        if (data.includes('League service not available')) {
            setUserFail(users[req.headers.index]);
            return console.log(data, users[req.headers.index]);

            // proxyRes.end('error message proxyRes');
        }
    });
    //    proxyRes.on('error', function (err, req, res) {
    //        setUserFail( users[req.headers.index]);

    //     console.log('wtf', err);
    // });
});

proxy.on('error', function(err, req, res) {
    try {
        res.writeHead(500, {
            'Content-Type': 'text/plain'
        });
    } catch (e) {}
    setUserFail(users[req.headers.index]);
    console.log('error', err);

    res.end('error message.');
});

console.log('listening on port', serverConfig.port);
server.listen(serverConfig.port);
setInterval(function() {
    console.log(serverName, 'Request for minute: ' + countrequestforminute + ' req');
    countrequestforminute = 0;
}, 60000);

function runLoop() {
    // superagent.get('http://localhost:2000/gettoken?user=MariaLynch&pass=abc123456')
    if (loopIsRunning) return console.log(serverName, 'Another loop running, abort!');
    loopIsRunning = true;
    console.log(serverName, 'Loop is running.... running clients -> ' + users.count('ready'));
    runNextClient();

    function runNextClient() {
        var nextClient = users.find(function(user) {
            return !user.status;
        });
        if (nextClient) {
            var port = serverConfig.internalPort + nextClient.index;
            initClient(
                nextClient.name,
                pass,
                SB_PREFIX + (nextClient.index + 1).toString(),
                port,
                function(err, res) {
                    if (!err) {
                        setUserConnect(nextClient);
                    }
                    runNextClient();
                }
            );
        } else {
            loopIsRunning = false;
            console.log('Loop is completed....');
        }
    }
}

function postUpdateAction() {
    console.log('i got here');
    var lcCheckSum = md5File.sync(LEAGUECLIENT_DIR + 'LeagueClient.exe');
    if (lcCheckSum !== MODIFIED_LEAGUECLIENT_MD5) {
        try {
            fs.moveSync(
                LEAGUECLIENT_DIR + 'LeagueClient.exe',
                LEAGUECLIENT_DIR + 'LeagueClient_original.exe',
                { overwrite: true }
            );
            fs.copySync('LeagueClient.exe', LEAGUECLIENT_DIR + 'LeagueClient.exe', {
                overwrite: true
            });
        } catch (err) {
            //Pusher.send('API server postUpdateAction: ' + err.message, err.stack, true);
        }
    }
}

function killLeagueByPort(port, cb) {
    ps.lookup(
        {
            arguments: '--app-port=' + port
        },
        function(err, resultList) {
            cb && cb();
            if (err) return;
            resultList.forEach(function(proc) {
                if (proc) {
                    console.log('killing', proc.pid);
                    ps.kill(proc.pid, function() {});
                }
            });
        }
    );
}

function initClient(user, pass, sbname, port, cb) {
    console.log('init client:', user, '- sb:', sbname);
    // clearsb(sbname);
    // stopApp(sbname);
    //backup stuff
    //try
    //{
    //    fs.copySync(LEAGUECLIENT_DIR + "LeagueClient_original.exe", "c:\\LeagueClient_original.exe", {overwrite: true});
    //} catch (err) {

    //}
    killLeagueByPort(port);

    var url =
        serverConfig.tokenServer +
        ':' +
        serverConfig.tokenPort +
        '/gettoken?new=true&user=' +
        user +
        '&pass=' +
        pass;
    superagent.get(url).end(function(err, res) {
        if (err || res.text === 'TRY AGAIN') {
            console.log(err || res.text);
            setTimeout(function() {
                cb(err || res.text);
            }, 5000);
        } else {
            var token = res.text;
            runLeague(token, sbname, port, serverConfig.region, cb);
        }
    });
}

function runLeague(token, sb, port, region, cb) {
    // postUpdateAction();
    console.log(token);
    // var runString = '"C:\\Program Files\\Sandboxie\\Start.exe"  /box:' + sb + ' C:\\LolAlpha\\GameData\\Apps\\LOLLCUVN\\LeagueClient\\LeagueClient.exe --locale=vn_VN --landing-url=https://lculanding.garena.vn/ --garena-plus-token='+token+' --servers.chat.chat_host=chatvn1.lol.garenanow.com --servers.lcds.lcds_host=prodvn1.lol.garenanow.com --servers.lcds.login_queue_url=https://lqvn1.lol.garenanow.com/login-queue/rest/queues/lol';
    // var runString = 'C:\\LolAlpha\\GameData\\Apps\\LOLLCUVN\\LeagueClient\\LeagueClient.exe --locale=vn_VN --landing-url=https://lculanding.garena.vn/ --app-listen-address=http://localhost:' +  port + ' --allow-multiple-clients --remoting-auth-token=123456 --use-http --headless --garena-plus-token='+token+' --servers.chat.chat_host=chatvn1.lol.garenanow.com --servers.lcds.lcds_host=prodvn1.lol.garenanow.com --servers.lcds.login_queue_url=https://lqvn1.lol.garenanow.com/login-queue/rest/queues/lol';
    //var runString = 'pm2 -f start --name "' + sb + '" -x C:\\LolAlpha\\GameData\\Apps\\LOLLCUVN\\LeagueClient\\LeagueClient_original.exe -- --locale=vn_VN --landing-url=https://lculanding.garena.vn/ --app-port=' + port + ' --use-http --headless --allow-multiple-clients --remoting-auth-token=530d6ba363b5a23e --garena-plus-token=' + token + ' --servers.chat.chat_host=chatvn1.lol.garenanow.com --servers.lcds.lcds_host=prodvn1.lol.garenanow.com --servers.lcds.login_queue_url=https://lqvn1.lol.garenanow.com/login-queue/rest/queues/lol';
    // var runString =
    //     'pm2 -o NULL -e NULL -f start --name "' +
    //     sb +
    //     '" -x "' +
    //     // LEAGUECLIENT_DIR +
    //     // 'LeagueClient_original.exe" -- --locale=vn_VN --region=' + region.toUpperCase() + ' --app-port=' +
    //     'LeagueClient.exe" -- --locale=vn_VN --region=' + region.toUpperCase() + ' --app-port=' +
    //     port +
    //     ' --headless --allow-multiple-clients --remoting-auth-token=530d6ba363b5a23e --garena-plus-token=' +
    //     //' --allow-multiple-clients --remoting-auth-token=530d6ba363b5a23e --garena-plus-token=' +
    //     token;

    // var runString = '"' +LEAGUECLIENT_DIR +
    //     'LeagueClient.exe" --locale=vn_VN --region=' + region.toUpperCase() + ' --app-port=' +
    //     port +
    //     ' --headless --allow-multiple-clients --remoting-auth-token=530d6ba363b5a23e --garena-plus-token=' +
    //     token;

    // console.log(runString);
    // var workingNewString = './LeagueClient.exe --locale=vn_VN --landing-url=https://lculanding.garena.vn/ --app-port=9999 --allow-multiple-clients --garena-plus-token=F8EuVg4ZSsuEsI0jalp2JHiQMRMZ/Im/p8rAlO1Z9kPTiZ6EtDKgcaXzz+MImLgKPqR+0VcgaEJSZ1jYyRbeeQq2o8eA6kCGUbY6dFdEfYzvtcLhd4SFYyt17b07p0fcn462H8Cx6whaLw73tjSkG6fo3Bxx0wCLbPVCBrjY39oMl2QuNoPxbiyY/3pdzZ8rzv4CWuQ1TSCVuFYHwmo2TwzqK3N5M+FfEdSST2JqB9w0RnW4Vll+Kxe09UZw4cyzH0Gu8IMwjRz4xj5iy6hp1A1K/ef8rIHTxHevlUxvl7EnMnvQiqeHSTgMmzcuBKGmcMVFXTRZUDO0WeoK8Ws6PA== --servers.chat.chat_host=chatvn1.lol.garenanow.com --servers.lcds.lcds_host=prodvn1.lol.garenanow.com --servers.lcds.login_queue_url=https://lqvn1.lol.garenanow.com/login-queue/rest/queues/lol';
    // var runString = 'pm2 -f start --name "' + sb+ '" -x C:\\LolAlpha\\GameData\\Apps\\LOLLCUVN\\LeagueClient\\LeagueClient.exe -- --locale=vn_VN --landing-url=https://lculanding.garena.vn/ --app-port=' + port + ' --headless --allow-multiple-clients --remoting-auth-token=530d6ba363b5a23e --garena-plus-token='+token+' --servers.chat.chat_host=chatvn1.lol.garenanow.com --servers.lcds.lcds_host=prodvn1.lol.garenanow.com --servers.lcds.login_queue_url=https://lqvn1.lol.garenanow.com/login-queue/rest/queues/lol';

    // console.log(runString);
    // cb(null);

    var runString =
        //'LeagueClient_original.exe" -- --locale=vn_VN --landing-url=https://lculanding.garena.vn/ --app-port=' +
        // 'LeagueClient_original.exe" -- --locale=vn_VN --app-port=' +
        'LeagueClient_original.exe --locale=vn_VN --app-port=' +
        port +
        ' --use-http --headless --allow-multiple-clients --remoting-auth-token=530d6ba363b5a23e --garena-plus-token=' +
        token +
        ' --servers.lcds.lcds_host=' +
        config[serverName].prod +
        ' --servers.lcds.login_queue_url=' +
        config[serverName].lq;
    //' --servers.chat.chat_host=chatvn1.lol.garenanow.com --servers.lcds.lcds_host=prodvn1.lol.garenanow.com --servers.lcds.login_queue_url=https://lqvn1.lol.garenanow.com/login-queue/rest/queues/lol';
    // console.log(runString);
    var args = [
        '--locale=en_SG',
        '--app-port=' + port,
        '--region=' +  region.toUpperCase(),
        //'--use-http',
        '--headless',
        '--allow-multiple-clients',
        '--remoting-auth-token=530d6ba363b5a23e',
        '--garena-plus-token=' + token,
        '--servers.lcds.lcds_host=' + config[serverName].prod,
        '--servers.lcds.login_queue_url=' + config[serverName].lq
    ];
    // exec(runString, function(error, stdout, stderr) {
    // exec(runString, { cwd: LEAGUECLIENT_DIR }, function(error, stdout, stderr) {
    //     // check if token can be created successfully
    //     console.log(stdout);
    //     if (error) {
    //         console.dir(error);
    //     }

    //     return cb(error);
    // });
    //var child = spawn('LeagueClient_original.exe', args, { cwd: LEAGUECLIENT_DIR });
    var child = spawn('LeagueClient.exe', args, { cwd: LEAGUECLIENT_DIR });
    child.stdout.on('data', data => {
        // console.log(`stdout: ${data}`);
    });

    child.stderr.on('data', data => {
        // console.log(`stderr: ${data}`);
    });

    cb(null);
}

function clearLogs() {
    var directory = LEAGUECLIENT_DIR + 'Logs\\LeagueClient Logs';
    fs.readdir(directory, function(err, files) {
        if (err) return err;

        for (var file of files) {
            fs.unlink(path.join(directory, file), function(err) {
                if (err) return err;
            });
        }
    });
}
function stopApp(name) {
    exec('pm2 stop ' + name);
    exec('pm2 delete ' + name);
}
function killAllLeague() {
    exec('taskkill /f /im LeagueClient.exe');
}

function killAlphaUx() {
    exec('pm2 kill');
}

function clearsb(name) {
    exec('"C:\\Program Files\\Sandboxie\\Start.exe" /box:' + name + ' /terminate');
}

//killAllLeague();
clearLogs();
runLoop();
setInterval(runLoop, 20000);