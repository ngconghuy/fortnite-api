const { CronJob } = require('cron');
const _ = require('lodash');
// const Config = require('./config');
const { GetCollection } = require('./collections');
const async = require('async');

const fetch = require('node-fetch');
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;
const callHttpApi = url => {
    return fetch(url, Config.vn.vnOptionsBuffer).then(function(res) {
        if (!res.ok) return res.statusText;
        return res.json();
    });
};




setTimeout(() => {
    // if (process.env.CRONABLE) {
    new CronJob({
        cronTime: '*/15 * * * *',
        onTick() {
            // statusCron();
        },
        start: true,
        runOnInit: true
        // timeZone: 'America/Los_Angeles'
    });
    // new CronJob({
    //     cronTime: '* * */3 * *',
    //     onTick() {
    //         statusCron();
    //     },
    //     start: true,
    //     runOnInit: true
    //     // timeZone: 'America/Los_Angeles'
    // });
    // }
}, 1000);
