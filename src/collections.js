/**
 * Created by nightshade on 1/19/17.
 */

const MongoClient = require('mongodb').MongoClient;

// Connection url
// const url = 'mongodb://localhost:3031/meteor';
const url =
    process.env.MONGO_URL ||
    'mongodb://localhost:27017/fortnite?readPreference=primary';
// // Connect using MongoClient
let _db;
let GetCollection = name => {
    return _db.collection(name);
};
MongoClient.connect(
    url,
    function(err, db) {
        _db = db;
        //add ensure indices here
        const dynamicCollections = [
            // {
            //     name: 'summoner-info',
            //     indices: [
            //         { internalName: 1, region: 1 },
            //         { 'data.summonerId': 1, region: 1 },
            //         { 'data.minusId': 1, region: 1 },
            //         { 'data.accountId': 1, region: 1 },
            //         { 'data.summonerLevel': 1, region: 1 }
            //     ]
            // },
            { name: 'player-info', indices: [{ playerId: 1, name: 1, platform: 1 }] },
            { name: 'player-stats-solo', indices: [{ playerId: 1 }] },
            { name: 'player-stats-duo', indices: [{ playerId: 1 }] },
            { name: 'player-stats-squad', indices: [{ playerId: 1 }] },
            { name: 'player-history', indices: [{ playerId: 1 }] },
        ];

        const EXPIRE = {
            ONE_MONTH: 60 * 60 * 24 * 30
        };

        dynamicCollections.forEach(({ name, indices }) => {
            global[name] = GetCollection(name);
            // indices.map(index => global[name]._ensureIndex(index));
            if (indices)
                indices.map(index => {
                    Array.isArray(index)
                        ? global[name].ensureIndex(index[0], index[1])
                        : global[name].ensureIndex(index);
                });
            // delete after one month
            global[name].ensureIndex({ lastUpdate: 1 }, { expireAfterSeconds: EXPIRE.ONE_MONTH });
        });
    }
);

module.exports.GetCollection = GetCollection;

// const beforeMethod = name => {
//     console.time(name);
// };

// const afterMethod = name => {
//     console.timeEnd(name);
// };

//benchmark method
// const meteorMethod = Meteor.methods;

// Meteor.methods = function(methods, guard) {
//     var methodsNames = _.keys(methods);
//     _.each(methodsNames, function(methodName) {
//         var method = {};
//         method[methodName] = function() {
//             beforeMethod(methodName);
//             var result = methods[methodName].apply(this, arguments);
//             Meteor.defer(function() {
//                 afterMethod(methodName);
//             });
//             return result;
//         };`
//         meteorMethod(method);
//     });
// };
