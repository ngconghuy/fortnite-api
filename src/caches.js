const Config = require('./config');
const fetch = require('node-fetch');
const { GetCollection } = require('./collections');
const _ = require('lodash');
// const DDPClient = require('ddp');
// const HttpsProxyAgent = require('https-proxy-agent');
// const agents = [
//     new HttpsProxyAgent(Config.proxies.newipnow[0]),
//     new HttpsProxyAgent(Config.proxies.newipnow[1]),
//     new HttpsProxyAgent(Config.proxies.newipnow[2])
// ];

// Adapter.emit({
//     async bundle() {
//         return bundle;
//     }
// });

const callHttpApi = (url, region) => {
    const opt = Config.vn.vnOptionsBuffer;
    // let opt;
    // if (Helpers.isSea(region || 'vn')) {
    //     opt = Config.vn.vnOptionsBuffer;
    // } else {
    //     opt = {
    //         ...Config.na.globalOptionsBuffer,
    //         agent: _.sample(agents)
    //     };
    // }
    return fetch(url, opt).then(function(res) {
        if (!res.ok) throw res.statusText;
        return res.json();
    });
};

const CACHE_TIME = {
    nocache: 1,
    '15sec': 15,
    '2mins': 60 * 2,
    '5mins': 60 * 5,
    '30mins': 60 * 30,
    '1hour': 60 * 60,
    '6hours': 60 * 60 * 6,
    '12hours': 60 * 60 * 12,
    '1day': 60 * 60 * 24,
    '2days': 60 * 60 * 24 * 2,
    '7days': 60 * 60 * 24 * 7,
    forever: Number.POSITIVE_INFINITY
};

const betaApiWrapper = async ({
    fetcher = callHttpApi,
    path,
    cachetime,
    collection,
    idName,
    idValue,
    transform,
    extraData,
    throwOnError,
    region,
    getInfoBySummId = false,
    updater = (colection, transformData, idName, idValue, region) => {
        global[collection].update(
            { [idName]: idValue, region },
            {
                [idName]: idValue,
                data: transformData,
                region,
                // ...extra,
                lastUpdate: new Date()
            },
            {
                upsert: true
            }
        );
    },
    checker = (collection, idName, idValue, region) =>
        GetCollection(collection)
            .findOne({ [idName]: idValue, region })
            .then(data => {
                return data;
            })
            .catch(e => {
                return e;
            }),
    acs = false
}) => {
    if (!cachetime) throw new Error('need cacheTime!');

    const url = !acs ? Config[region.toLowerCase()].api + path : path;
    // console.log(idValue);
    const cacheData = await checker(collection, idName, idValue, region);

    const now = new Date();
    if (
        cacheData &&
        cacheData.lastUpdate &&
        now.getTime() - cacheData.lastUpdate.getTime() < cachetime * 1000
    ) {
        return cacheData.data;
    }

    try {
        const data = await fetcher(url);
        if (_.isObject(data) || _.isArray(data) || _.isNumber(data)) {
            const transformData = transform
                ? await transform(data, region, cacheData ? cacheData.data : undefined)
                : data;
            await updater(collection, transformData, idName, idValue, region);
            return transformData;
        } else {
            if (!throwOnError && cacheData) return cacheData.data;
            else {
                throw new Error(collection);
            }
        }
    } catch (e) {
        if (!throwOnError && cacheData) return cacheData.data;
        else throw new Error(e, collection);
        // return cache data on error
    }
};

module.exports = { betaApiWrapper };
